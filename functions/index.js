const functions = require('firebase-functions');

// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

/**
 * Reports endpoint
 */

//2. How many days pass between reports made by the same user? (Dashboard app)
//How many days pass between
// {{baseURL}}/averageSecondsBetweenReports/{{userID}}
exports.averageSecondsBetweenReports = functions.https.onRequest(
  async (req, res) => {
    params = req.params[0].split('/');

    var average = -1;
    lastTimestamp = null;
    reportTimestamp = null;
    var n = 0;
    var difference = 0;

    query = await admin
      .firestore()
      .collection('reports')
      .where('author', '==', params[0])
      .orderBy('created', 'asc')
      .get();

    const docs = query.docs.map((doc) => doc.data());
    docs.map((report) => {
      reportTimestamp = report.created.seconds;
      if (lastTimestamp) {
        console.log(reportTimestamp, lastTimestamp);
        difference = difference + reportTimestamp - lastTimestamp;
        n++;
        console.log(difference, n);
      }
      lastTimestamp = reportTimestamp;
    });
    if (n > 0) {
      console.log(difference, n);
      average = difference / n;
    }

    res.status(200).send({ average });
  }
);

//Days since last report user
// {{baseURL}}/daysSinceLastReport/{{userID}}
exports.daysSinceLastReport = functions.https.onRequest(async (req, res) => {
  params = req.params[0].split('/');

  lastTimestamp = null;

  query = await admin
    .firestore()
    .collection('reports')
    .where('author', '==', params[0])
    .orderBy('created', 'asc')
    .get();

  const docs = query.docs.map((doc) => doc.data());
  //console.log(docs);

  lastTimestamp = docs[docs.length - 1].created;
  const lastDate = lastTimestamp.toDate();
  console.log('lasttimestamp', lastTimestamp);
  console.log('lastdate', lastDate.toString());

  const currentTime = new Date();
  console.log('current time', currentTime.toString());

  let differenceTime = currentTime.getTime() - lastDate.getTime();
  console.log('difference', differenceTime);

  const daysSinceLastReport = differenceTime / (1000 * 3600 * 24);
  console.log('days', daysSinceLastReport);

  res.status(200).send({ daysSinceLastReport });
});

//2. Which reports a user created have/haven’t already been fixed? (Dashboard app)
// {{baseURL}}/reportsNotFixed/{{userID}}
exports.reportsNotFixed = functions.https.onRequest(async (req, res) => {
  params = req.params[0].split('/');
  query = await admin
    .firestore()
    .collection('reports')
    .where('author', '==', params[0])
    .where('status', '!=', 'Closed')
    .get();
  var docs = query.docs.map((doc) => doc.data());
  res.status(200).send({ reports: docs, numberOfReports: docs.length });
});

//Reports made by a user
// {{baseURL}}/reportsMade/{{userID}}
exports.reportsMade = functions.https.onRequest(async (req, res) => {
  params = req.params[0].split('/');
  query = await admin
    .firestore()
    .collection('reports')
    .where('author', '==', params[0])
    .get();
  var docs = query.docs.map((doc) => doc.data());
  res.status(200).send({ reports: docs, numberOfReports: docs.length });
});

exports.dashboardYourReports = functions.https.onRequest(async (req, res) => {
  const author = req.query.author;

  queryReportsMade = await admin
    .firestore()
    .collection('reports')
    .where('author', '==', author)
    .get();

  var docsReportsMade = queryReportsMade.docs.map((doc) => doc.data());

  lastTimestamp = null;

  queryDaysSinceLastReport = await admin
    .firestore()
    .collection('reports')
    .where('author', '==', author)
    .orderBy('created', 'asc')
    .get();

  const docsDaysSinceLastReport = queryDaysSinceLastReport.docs.map((doc) =>
    doc.data()
  );

  lastTimestamp =
    docsDaysSinceLastReport[docsDaysSinceLastReport.length - 1].created;
  const lastDate = lastTimestamp.toDate();
  const currentTime = new Date();

  let differenceTime = currentTime.getTime() - lastDate.getTime();

  const daysSinceLastReport = differenceTime / (1000 * 3600 * 24);

  const queryReportsNotFixed = await admin
    .firestore()
    .collection('reports')
    .where('author', '==', author)
    .where('status', '!=', 'Closed')
    .get();
  var docsReportsNotFixed = queryReportsNotFixed.docs.map((doc) => doc.data());

  res.status(200).send({
    reportsMade: docsReportsMade.length,
    daysSinceLastReport: daysSinceLastReport,
    repairPercentage: 1 - docsReportsNotFixed.length / docsReportsMade.length,
    averageRepairTime: -1,
  });
});
